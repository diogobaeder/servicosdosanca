POETRY = poetry run
MANAGE = $(POETRY) python manage.py

run-dev: dev-env
	@$(MANAGE) runserver

migrate:
	@$(MANAGE) migrate

collect-static:
	@$(MANAGE) collectstatic --noinput --clear

start-production: migrate
	@mkdir -p /tmp/servicosdosanca
	@$(POETRY) uwsgi uwsgi.ini

dev-env:
	$(eval export DJANGO_SETTINGS_MODULE=servicosdosanca.settings.dev)

production-env:
	$(eval export DJANGO_SETTINGS_MODULE=servicosdosanca.settings.production)
