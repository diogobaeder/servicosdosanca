# Use an official Python runtime based on Debian 10 "buster" as a parent image.
FROM python:3.10-slim-buster

# Add user that will be used in the container.
RUN useradd www-data

# Port used by this container to serve HTTP.
EXPOSE 8000

# Set environment variables.
# 1. Force Python stdout and stderr streams to be unbuffered.
# 2. Set PORT variable that is used by Gunicorn. This should match "EXPOSE"
#    command.
ENV PYTHONUNBUFFERED=1 \
    DJANGO_SETTINGS_MODULE=servicosdosanca.settings.production \
    PORT=8000

# Install system packages required by Wagtail and Django.
RUN apt-get update --yes --quiet && apt-get install --yes --quiet --no-install-recommends \
    build-essential \
    libpq-dev \
    libmariadbclient-dev \
    libjpeg62-turbo-dev \
    zlib1g-dev \
    libwebp-dev \
 && rm -rf /var/lib/apt/lists/*

# Install the application server.
RUN pip install poetry

# Install the project requirements.
COPY poetry.lock /
RUN poetry install

# Use /app folder as a directory where the source code is stored.
WORKDIR /var/www/servicosdosanca/

# Set this directory to be owned by the "wagtail" user. This Wagtail project
# uses SQLite, the folder needs to be owned by the user that
# will be writing to the database file.
RUN chown www-data:www-data /var/www/servicosdosanca/

# Copy the source code of the project into the container.
COPY --chown=www-data:www-data . .

# Use user "wagtail" to run the build commands below and the server itself.
USER www-data

# Collect static files.
RUN make collect-static

CMD make start-production
